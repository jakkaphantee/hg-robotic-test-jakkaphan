import {
	SHOW_ADD_MEASUREMENT_MODAL
} from './types'

const state = () => ({
	isAddMeasurementModalOpen: false
})

const mutations = {
	[SHOW_ADD_MEASUREMENT_MODAL]: (state, value) => {
		state.isAddMeasurementModalOpen = value
	}
}

export {
	state,
	mutations
}
