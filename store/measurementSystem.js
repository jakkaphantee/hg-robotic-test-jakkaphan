import {
	getMeasurementSystem,
	postMeasurementSystem
} from '@/service/api/measurementSystem.api'

import {
	SELECT_MEASUREMENT_SYSTEM,
	GET_MEASUREMENT_SYSTEM_REQUEST,
	GET_MEASUREMENT_SYSTEM_SUCCESS,
	GET_MEASUREMENT_SYSTEM_FAILURE,
	POST_MEASUREMENT_SYSTEM_REQUEST,
	POST_MEASUREMENT_SYSTEM_SUCCESS,
	POST_MEASUREMENT_SYSTEM_FAILURE
} from './types'

const state = () => ({
	currentMeasurementSystem: {},
	measurementSystemList: {
		isLoading: false,
		isSuccess: false,
		data: [],
		errorMessage: ''
	},
	createMeasurementSystem: {
		isLoading: false,
		isSuccess: false,
		data: {},
		errorMessage: ''
	}
})

const mutations = {
	[SELECT_MEASUREMENT_SYSTEM]: (state, measurementSystem) => {
		state.currentMeasurementSystem = { ...measurementSystem }
	},
	[GET_MEASUREMENT_SYSTEM_REQUEST]: (state) => {
		state.measurementSystemList = {
			...state.measurementSystemList,
			isLoading: true,
			isSuccess: false,
			data: []
		}
	},
	[GET_MEASUREMENT_SYSTEM_SUCCESS]: (state, response) => {
		state.measurementSystemList = {
			...state.measurementSystemList,
			isLoading: false,
			isSuccess: true,
			data: response
		}
		if (!state.currentMeasurementSystem.id && response.length > 0) {
			state.currentMeasurementSystem = response[0]
		}
	},
	[GET_MEASUREMENT_SYSTEM_FAILURE]: (state, error) => {
		state.measurementSystemList = {
			...state.measurementSystemList,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	},
	[POST_MEASUREMENT_SYSTEM_REQUEST]: (state) => {
		state.createMeasurementSystem = {
			...state.createMeasurementSystem,
			isLoading: true,
			isSuccess: false
		}
	},
	[POST_MEASUREMENT_SYSTEM_SUCCESS]: (state, response) => {
		state.createMeasurementSystem = {
			...state.createMeasurementSystem,
			isLoading: false,
			isSuccess: true,
			data: response
		}
	},
	[POST_MEASUREMENT_SYSTEM_FAILURE]: (state, error) => {
		state.createMeasurementSystem = {
			...state.createMeasurementSystem,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.messsage
		}
	}
}

const actions = {
	getMeasurementSystem: async ({ commit }) => {
		commit(GET_MEASUREMENT_SYSTEM_REQUEST)
		try {
			const { data: response } = await getMeasurementSystem()
			commit(GET_MEASUREMENT_SYSTEM_SUCCESS, response)
		} catch (error) {
			commit(GET_MEASUREMENT_SYSTEM_FAILURE, error)
		}
	},
	postMeasurementSystem: async ({ commit }, { name }) => {
		commit(POST_MEASUREMENT_SYSTEM_REQUEST)
		try {
			const { data: response } = await postMeasurementSystem({ name })
			commit(POST_MEASUREMENT_SYSTEM_SUCCESS, response)
		} catch (error) {
			commit(POST_MEASUREMENT_SYSTEM_FAILURE, error)
		}
	}
}

export {
	state,
	mutations,
	actions
}
