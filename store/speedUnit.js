import {
	getSpeedUnit,
	postSpeedUnit
} from '@/service/api/speedUnit.api'

import {
	SELECT_SPEED_UNIT,
	GET_SPEED_UNIT_REQUEST,
	GET_SPEED_UNIT_SUCCESS,
	GET_SPEED_UNIT_FAILURE,
	POST_SPEED_UNIT_REQUEST,
	POST_SPEED_UNIT_SUCCESS,
	POST_SPEED_UNIT_FAILURE
} from './types'

const state = () => ({
	currentSpeedUnit: {},
	speedUnitList: {
		isLoading: false,
		isSuccess: false,
		data: [],
		errorMessage: ''
	},
	createSpeedUnit: {
		isLoading: false,
		isSuccess: false,
		errorMessage: ''
	}
})

const mutations = {
	[SELECT_SPEED_UNIT]: (state, speedUnit) => {
		state.currentSpeedUnit = { ...speedUnit }
	},
	[GET_SPEED_UNIT_REQUEST]: (state) => {
		state.speedUnitList = {
			...state.speedUnitList,
			isLoading: true,
			isSuccess: false,
			data: []
		}
	},
	[GET_SPEED_UNIT_SUCCESS]: (state, response) => {
		state.speedUnitList = {
			...state.speedUnitList,
			isLoading: false,
			isSuccess: true,
			data: response
		}
	},
	[GET_SPEED_UNIT_FAILURE]: (state, error) => {
		state.speedUnitList = {
			...state.speedUnitList,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	},
	[POST_SPEED_UNIT_REQUEST]: (state) => {
		state.createSpeedUnit = {
			...state.createSpeedUnit,
			isLoading: true,
			isSuccess: false
		}
	},
	[POST_SPEED_UNIT_SUCCESS]: (state) => {
		state.createSpeedUnit = {
			...state.createSpeedUnit,
			isLoading: false,
			isSuccess: true
		}
	},
	[POST_SPEED_UNIT_FAILURE]: (state, error) => {
		state.createSpeedUnit = {
			...state.createSpeedUnit,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	}
}

const actions = {
	getSpeedUnit: async ({ commit, dispatch }) => {
		commit(GET_SPEED_UNIT_REQUEST)
		try {
			const { data: response } = await getSpeedUnit()
			commit(GET_SPEED_UNIT_SUCCESS, response)
			dispatch('setDefaultSpeed')
		} catch (error) {
			commit(GET_SPEED_UNIT_FAILURE, error)
		}
	},
	postSpeedUnit: async ({ commit }, { measurementId, name, multiplier }) => {
		commit(POST_SPEED_UNIT_REQUEST)
		try {
			const { data: response } = await postSpeedUnit({ measurementId, name, multiplier })
			commit(POST_SPEED_UNIT_SUCCESS, response)
		} catch (error) {
			commit(POST_SPEED_UNIT_FAILURE, error)
		}
	},
	setDefaultSpeed: ({ commit, state, rootState }) => {
		const speedUnitList = state.speedUnitList.data
		const currentMeasurementSystem = rootState.measurementSystem.currentMeasurementSystem
		const currentSpeedUnit = speedUnitList.find(speedUnit => speedUnit.measurementId === currentMeasurementSystem.id)
		commit(SELECT_SPEED_UNIT, currentSpeedUnit)
	}
}

export {
	state,
	mutations,
	actions
}
