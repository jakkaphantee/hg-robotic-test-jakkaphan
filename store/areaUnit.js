import {
	getAreaUnit,
	postAreaUnit
} from '@/service/api/areaUnit.api'

import {
	SELECT_AREA_UNIT,
	GET_AREA_UNIT_REQUEST,
	GET_AREA_UNIT_SUCCESS,
	GET_AREA_UNIT_FAILURE,
	POST_AREA_UNIT_REQUEST,
	POST_AREA_UNIT_SUCCESS,
	POST_AREA_UNIT_FAILURE
} from './types'

const state = () => ({
	currentAreaUnit: {},
	areaUnitList: {
		isLoading: false,
		isSuccess: false,
		data: [],
		errorMessage: ''
	},
	createAreaUnit: {
		isLoading: false,
		isSuccess: false,
		errorMessage: ''
	}
})

const mutations = {
	[SELECT_AREA_UNIT]: (state, areaUnit) => {
		state.currentAreaUnit = { ...areaUnit }
	},
	[GET_AREA_UNIT_REQUEST]: (state) => {
		state.areaUnitList = {
			...state.areaUnitList,
			isLoading: true,
			isSuccess: false,
			data: []
		}
	},
	[GET_AREA_UNIT_SUCCESS]: (state, response) => {
		state.areaUnitList = {
			...state.areaUnitList,
			isLoading: false,
			isSuccess: true,
			data: response
		}
	},
	[GET_AREA_UNIT_FAILURE]: (state, error) => {
		state.areaUnitList = {
			...state.areaUnitList,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	},
	[POST_AREA_UNIT_REQUEST]: (state) => {
		state.createAreaUnit = {
			...state.createAreaUnit,
			isLoading: true,
			isSuccess: false
		}
	},
	[POST_AREA_UNIT_SUCCESS]: (state) => {
		state.createAreaUnit = {
			...state.createAreaUnit,
			isLoading: false,
			isSuccess: true
		}
	},
	[POST_AREA_UNIT_FAILURE]: (state, error) => {
		state.createAreaUnit = {
			...state.createAreaUnit,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.messsage
		}
	}
}

const actions = {
	getAreaUnit: async ({ commit, dispatch }) => {
		commit(GET_AREA_UNIT_REQUEST)
		try {
			const { data: response } = await getAreaUnit()
			commit(GET_AREA_UNIT_SUCCESS, response)
			dispatch('setDefaultArea')
		} catch (error) {
			commit(GET_AREA_UNIT_FAILURE, error)
		}
	},
	postAreaUnit: async ({ commit }, { measurementId, name, multiplier }) => {
		commit(POST_AREA_UNIT_REQUEST)
		try {
			const { data: response } = await postAreaUnit({ measurementId, name, multiplier })
			commit(POST_AREA_UNIT_SUCCESS, response)
		} catch (error) {
			commit(POST_AREA_UNIT_FAILURE, error)
		}
	},
	setDefaultArea: ({ commit, state, rootState }) => {
		const areaUnitList = state.areaUnitList.data
		const currentMeasurementSystem = rootState.measurementSystem.currentMeasurementSystem
		const currentAreaUnit = areaUnitList.find(areaUnit => areaUnit.measurementId === currentMeasurementSystem.id)
		commit(SELECT_AREA_UNIT, currentAreaUnit)
	}
}

export {
	state,
	mutations,
	actions
}
