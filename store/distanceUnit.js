import {
	getDistanceUnit,
	postDistanceUnit
} from '@/service/api/distanceUnit.api'

import {
	SELECT_DISTANCE_UNIT,
	GET_DISTANCE_UNIT_REQUEST,
	GET_DISTANCE_UNIT_SUCCESS,
	GET_DISTANCE_UNIT_FAILURE,
	POST_DISTANCE_UNIT_REQUEST,
	POST_DISTANCE_UNIT_SUCCESS,
	POST_DISTANCE_UNIT_FAILURE
} from './types'

const state = () => ({
	currentDistanceUnit: {},
	distanceUnitList: {
		isLoading: false,
		isSuccess: false,
		data: [],
		errorMessage: ''
	},
	createDistanceUnit: {
		isLoading: false,
		isSuccess: false,
		errorMessage: ''
	}
})

const mutations = {
	[SELECT_DISTANCE_UNIT]: (state, distanceUnit) => {
		state.currentDistanceUnit = { ...distanceUnit }
	},
	[GET_DISTANCE_UNIT_REQUEST]: (state) => {
		state.distanceUnitList = {
			...state.distanceUnitList,
			isLoading: true,
			isSuccess: false,
			data: []
		}
	},
	[GET_DISTANCE_UNIT_SUCCESS]: (state, response) => {
		state.distanceUnitList = {
			...state.distanceUnitList,
			isLoading: false,
			isSuccess: true,
			data: response
		}
	},
	[GET_DISTANCE_UNIT_FAILURE]: (state, error) => {
		state.distanceUnitList = {
			...state.distanceUnitList,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	},
	[POST_DISTANCE_UNIT_REQUEST]: (state) => {
		state.createDistanceUnit = {
			...state.createDistanceUnit,
			isLoading: true,
			isSuccess: false
		}
	},
	[POST_DISTANCE_UNIT_SUCCESS]: (state) => {
		state.createDistanceUnit = {
			...state.createDistanceUnit,
			isLoading: false,
			isSuccess: true
		}
	},
	[POST_DISTANCE_UNIT_FAILURE]: (state, error) => {
		state.createDistanceUnit = {
			...state.createDistanceUnit,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	}
}

const actions = {
	getDistanceUnit: async ({ commit, dispatch }) => {
		commit(GET_DISTANCE_UNIT_REQUEST)
		try {
			const { data: response } = await getDistanceUnit()
			commit(GET_DISTANCE_UNIT_SUCCESS, response)
			dispatch('setDefaultDistance')
		} catch (error) {
			commit(GET_DISTANCE_UNIT_FAILURE, error)
		}
	},
	postDistanceUnit: async ({ commit }, { measurementId, name, multiplier }) => {
		commit(POST_DISTANCE_UNIT_REQUEST)
		try {
			const { data: response } = await postDistanceUnit({ measurementId, name, multiplier })
			commit(POST_DISTANCE_UNIT_SUCCESS, response)
		} catch (error) {
			commit(POST_DISTANCE_UNIT_FAILURE, error)
		}
	},
	setDefaultDistance: ({ commit, state, rootState }) => {
		const distanceUnitList = state.distanceUnitList.data
		const currentMeasurementSystem = rootState.measurementSystem.currentMeasurementSystem
		const currentDistanceUnit = distanceUnitList.find(distanceUnit => distanceUnit.measurementId === currentMeasurementSystem.id)
		commit(SELECT_DISTANCE_UNIT, currentDistanceUnit)
	}
}

export {
	state,
	mutations,
	actions
}
