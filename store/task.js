import {
	GET_TASK_LIST_REQUEST,
	GET_TASK_LIST_SUCCESS,
	GET_TASK_LIST_FAILURE
} from './types'

const state = () => ({
	taskList: {
		isLoading: false,
		isSuccess: false,
		data: []
	}
})

const getters = {
	//
}

const mutations = {
	[GET_TASK_LIST_REQUEST]: (state) => {
		state.taskList = {
			...state.taskList,
			isLoading: true,
			isSuccess: false
		}
	},
	[GET_TASK_LIST_SUCCESS]: (state, response) => {
		state.taskList = {
			...state.taskList,
			isLoading: false,
			isSuccess: true,
			data: response.data
		}
	},
	[GET_TASK_LIST_FAILURE]: (state) => {
		state.taskList = {
			...state.taskList,
			isLoading: false,
			isSuccess: false,
			data: []
		}
	}
}

const actions = {
	getTaskList: ({ commit }) => {
		commit(GET_TASK_LIST_REQUEST)
		try {
			const response = {
				data: []
			}
			commit(GET_TASK_LIST_SUCCESS, response)
		} catch (error) {
			commit(GET_TASK_LIST_FAILURE)
		}
	}
}

export {
	state,
	getters,
	mutations,
	actions
}
