import {
	getVolumeUnit,
	postVolumeUnit
} from '@/service/api/volumeUnit.api'

import {
	SELECT_VOLUME_UNIT,
	GET_VOLUME_UNIT_REQUEST,
	GET_VOLUME_UNIT_SUCCESS,
	GET_VOLUME_UNIT_FAILURE,
	POST_VOLUME_UNIT_REQUEST,
	POST_VOLUME_UNIT_SUCCESS,
	POST_VOLUME_UNIT_FAILURE
} from './types'

const state = () => ({
	currentVolumeUnit: {},
	volumeUnitList: {
		isLoading: false,
		isSuccess: false,
		data: [],
		errorMessage: ''
	},
	createVolumeUnit: {
		isLoading: false,
		isSuccess: false,
		errorMessage: ''
	}
})

const mutations = {
	[SELECT_VOLUME_UNIT]: (state, volumeUnit) => {
		state.currentVolumeUnit = { ...volumeUnit }
	},
	[GET_VOLUME_UNIT_REQUEST]: (state) => {
		state.volumeUnitList = {
			...state.volumeUnitList,
			isLoading: true,
			isSuccess: false,
			data: []
		}
	},
	[GET_VOLUME_UNIT_SUCCESS]: (state, response) => {
		state.volumeUnitList = {
			...state.volumeUnitList,
			isLoading: false,
			isSuccess: true,
			data: response
		}
	},
	[GET_VOLUME_UNIT_FAILURE]: (state, error) => {
		state.volumeUnitList = {
			...state.volumeUnitList,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	},
	[POST_VOLUME_UNIT_REQUEST]: (state) => {
		state.createVolumeUnit = {
			...state.createVolumeUnit,
			isLoading: true,
			isSuccess: false
		}
	},
	[POST_VOLUME_UNIT_SUCCESS]: (state) => {
		state.createVolumeUnit = {
			...state.createVolumeUnit,
			isLoading: false,
			isSuccess: true
		}
	},
	[POST_VOLUME_UNIT_FAILURE]: (state, error) => {
		state.createVolumeUnit = {
			...state.createVolumeUnit,
			isLoading: false,
			isSuccess: false,
			errorMessage: error.message
		}
	}
}

const actions = {
	getVolumeUnit: async ({ commit, dispatch }) => {
		commit(GET_VOLUME_UNIT_REQUEST)
		try {
			const { data: response } = await getVolumeUnit()
			commit(GET_VOLUME_UNIT_SUCCESS, response)
			dispatch('setDefaultVolume')
		} catch (error) {
			commit(GET_VOLUME_UNIT_FAILURE, error)
		}
	},
	postVolumeUnit: async ({ commit }, { measurementId, name, multiplier }) => {
		commit(POST_VOLUME_UNIT_REQUEST)
		try {
			const { data: response } = await postVolumeUnit({ measurementId, name, multiplier })
			commit(POST_VOLUME_UNIT_SUCCESS, response)
		} catch (error) {
			commit(POST_VOLUME_UNIT_FAILURE, error)
		}
	},
	setDefaultVolume: ({ commit, state, rootState }) => {
		const volumeUnitList = state.volumeUnitList.data
		const currentMeasurementSystem = rootState.measurementSystem.currentMeasurementSystem
		const currentVolumeUnit = volumeUnitList.find(volumeUnit => volumeUnit.measurementId === currentMeasurementSystem.id)
		commit(SELECT_VOLUME_UNIT, currentVolumeUnit)
	}
}

export {
	state,
	mutations,
	actions
}
