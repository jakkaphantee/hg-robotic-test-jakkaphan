import httpClient from './axiosInstance'

const END_POINT = '/measurement-system'

const getMeasurementSystem = () => httpClient({
	method: 'get',
	url: END_POINT
})

const postMeasurementSystem = ({ name }) => httpClient({
	method: 'post',
	url: END_POINT,
	data: {
		name
	}
})

export {
	getMeasurementSystem,
	postMeasurementSystem
}
