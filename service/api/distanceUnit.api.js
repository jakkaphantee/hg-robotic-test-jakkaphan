import httpClient from './axiosInstance'

const END_POINT = '/distance-unit'

const getDistanceUnit = () => httpClient({
	method: 'get',
	url: END_POINT
})

const postDistanceUnit = ({ measurementId, name, multiplier }) => httpClient({
	method: 'post',
	url: END_POINT,
	data: {
		measurementId,
		name,
		multiplier
	}
})

export {
	getDistanceUnit,
	postDistanceUnit
}
