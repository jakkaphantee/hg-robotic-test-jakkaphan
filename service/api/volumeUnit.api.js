import httpClient from './axiosInstance'

const END_POINT = '/volume-unit'

const getVolumeUnit = () => httpClient({
	method: 'get',
	url: END_POINT
})

const postVolumeUnit = ({ measurementId, name, multiplier }) => httpClient({
	method: 'post',
	url: END_POINT,
	data: {
		measurementId,
		name,
		multiplier
	}
})

export {
	getVolumeUnit,
	postVolumeUnit
}
