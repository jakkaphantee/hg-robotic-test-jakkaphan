import httpClient from './axiosInstance'

const END_POINT = '/speed-unit'

const getSpeedUnit = () => httpClient({
	method: 'get',
	url: END_POINT
})

const postSpeedUnit = ({ measurementId, name, multiplier }) => httpClient({
	method: 'post',
	url: END_POINT,
	data: {
		measurementId,
		name,
		multiplier
	}
})

export {
	getSpeedUnit,
	postSpeedUnit
}
