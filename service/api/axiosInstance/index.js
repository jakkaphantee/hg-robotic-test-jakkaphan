import axios from 'axios'
import humps from 'humps'

const axiosInstance = axios.create({
	baseURL: '/api'
})

const requestInterceptor = (config) => {
	if (config.data) {
		config.data = humps.decamelizeKeys(config.data)
	}
	if (config.params) {
		config.params = humps.decamelizeKeys(config.params)
	}
	return config
}

const requestInterceptorError = error => new Error(error)

const responseInterceptor = (response) => {
	return humps.camelizeKeys(response)
}

const responseInterceptorError = error => new Error(error)

axiosInstance.interceptors.request.use(requestInterceptor, requestInterceptorError)
axiosInstance.interceptors.response.use(responseInterceptor, responseInterceptorError)

export default axiosInstance
