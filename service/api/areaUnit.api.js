import httpClient from './axiosInstance'

const END_POINT = '/area-unit'

const getAreaUnit = () => httpClient({
	method: 'get',
	url: END_POINT
})

const postAreaUnit = ({ measurementId, name, multiplier }) => httpClient({
	method: 'post',
	url: END_POINT,
	data: {
		measurementId,
		name,
		multiplier
	}
})

export {
	getAreaUnit,
	postAreaUnit
}
