import express from 'express'
import { fireStore } from './firebaseInstance'

const router = express()

const PATH = '/speed-unit'
const COLLECTION = 'speed_unit'

router.get(PATH, async (req, res) => {
	try {
		const response = await fireStore.collection(COLLECTION).get()
		const speedUnitList = []
		response.forEach((querySnapshot) => {
			if (querySnapshot.data()) {
				speedUnitList.push({
					...querySnapshot.data(),
					id: querySnapshot.id
				})
			}
		})
		res.status(200).send(speedUnitList)
	} catch (error) {
		res.status(error.status).send(error)
	}
})

router.post(PATH, async (req, res) => {
	try {
		await fireStore.collection(COLLECTION).doc().set({
			...req.body
		})
		res.status(200).send({ success: true })
	} catch (error) {
		res.status(error.status).send(error)
	}
})

export default router
