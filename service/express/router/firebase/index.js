import measurementSystem from './measurementSystem'
import areaUnit from './areaUnit'
import distanceUnit from './distanceUnit'
import speedUnit from './speedUnit'
import volumeUnit from './volumeUnit'

export default (router) => {
	router.use(measurementSystem)
	router.use(areaUnit)
	router.use(distanceUnit)
	router.use(speedUnit)
	router.use(volumeUnit)
}
