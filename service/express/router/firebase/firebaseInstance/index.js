import firebase from 'firebase'

const firebaseConfig = {
	apiKey: 'AIzaSyDBy85EtMs1nM0RDRDKeLii-JT8A951TgY',
	authDomain: 'hg-robotic-interview-test.firebaseapp.com',
	projectId: 'hg-robotic-interview-test',
	storageBucket: 'hg-robotic-interview-test.appspot.com',
	messagingSenderId: '1092013731471',
	appId: '1:1092013731471:web:f617bc5103f5d298d88afa'
}

if (!firebase.apps.length) {
	firebase.initializeApp(firebaseConfig)
}

export const fireStore = firebase.firestore()
