import express from 'express'
import { fireStore } from './firebaseInstance'

const router = express()

const PATH = '/measurement-system'
const COLLECTION = 'measurement_system'

router.get(PATH, async (req, res) => {
	try {
		const response = await fireStore.collection(COLLECTION).get()
		const measurementList = []
		response.forEach((querySnapshot) => {
			if (querySnapshot.data()) {
				const data = {
					...querySnapshot.data(),
					id: querySnapshot.id
				}
				measurementList.push(data)
			}
		})
		res.status(200).send(measurementList)
	} catch (error) {
		res.status(error.status).send(error)
	}
})

router.post(PATH, async (req, res) => {
	if (Object.keys(req.body).length) {
		try {
			const doc = fireStore.collection(COLLECTION).doc()
			await doc.set({
				...req.body
			})
			res.status(200).send({ id: doc.id })
		} catch (error) {
			res.status(error.status).send(error)
		}
	} else {
		res.status(400).send({ status: 400, message: 'Bad Request' })
	}
})

export default router
