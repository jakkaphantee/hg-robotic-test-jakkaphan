import express from 'express'
import { fireStore } from './firebaseInstance'

const router = express()

const PATH = '/area-unit'
const COLLECTION = 'area_unit'

router.get(PATH, async (req, res) => {
	try {
		const response = await fireStore.collection(COLLECTION).get()
		const areaUnitList = []
		response.forEach((querySnapshot) => {
			if (querySnapshot.data()) {
				const data = {
					...querySnapshot.data(),
					id: querySnapshot.id
				}
				areaUnitList.push(data)
			}
		})
		res.status(200).send(areaUnitList)
	} catch (error) {
		res.status(error.status).send(error)
	}
})

router.post(PATH, async (req, res) => {
	try {
		await fireStore.collection(COLLECTION).doc().set({
			...req.body
		})
		res.status(200).send({ success: true })
	} catch (error) {
		res.status(error.status).send(error)
	}
})

export default router
