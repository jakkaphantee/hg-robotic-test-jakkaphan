import express from 'express'
import bodyParser from 'body-parser'
import firebaseRouter from './router/firebase'

const router = express()

router.use(bodyParser.json({ limit: '50mb' }))
router.use(
	bodyParser.urlencoded({
		limit: '50mb',
		extended: true,
		parameterLimit: 50000
	})
)

firebaseRouter(router)

export default {
	path: '/api',
	handler: router
}
