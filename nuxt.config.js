export default {
	head: {
		title: 'HG-Robotic-Test',
		htmlAttrs: {
			lang: 'en'
		},
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: '' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},
	css: [
		'~/assets/styles/main.scss'
	],
	plugins: [
	],
	buildModules: [
		'@nuxtjs/eslint-module'
	],
	modules: [
		'bootstrap-vue/nuxt'
	],
	build: {
	},
	server: {
		port: 8000
	},
	serverMiddleware: [
		'~/service/express'
	]
}
